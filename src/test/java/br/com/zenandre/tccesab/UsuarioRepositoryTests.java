package br.com.zenandre.tccesab;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.zenandre.tccesab.domain.Usuario;
import br.com.zenandre.tccesab.repository.UsuarioRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsuarioRepositoryTests {
	
	@Autowired
	private UsuarioRepository repository;

	@Test
	public void test1() {
		Usuario usuario = new Usuario();
		usuario.setId(1);
		usuario.setEmail("usuario@usuarios.com");
		usuario.setNome("Usuário de teste");
		usuario.setMatricula("111111111");
		usuario.setTelefone("+5585988888888");
		
		Usuario usuarioSalvo = this.repository.save(usuario);
		assertNotNull(usuarioSalvo);
	}
	
	@Test
	public void test2() {
		Usuario usuario = new Usuario();
		usuario.setId(1);
		usuario.setEmail("usuario@usuarios.com");
		usuario.setNome("Usuário de teste");
		usuario.setMatricula("111111111");
		usuario.setTelefone("+5585988888888");
		
		Usuario usuarioSalvo = this.repository.save(usuario);
		assertNotNull(usuarioSalvo);
	}
	
	
	@Test
	public void test3() {
		Usuario usuario = new Usuario();
		usuario.setId(1);
		usuario.setEmail("usuario@usuarios.com");
		usuario.setNome("Usuário de teste");
		usuario.setMatricula("111111111");
		usuario.setTelefone("+5585988888888");
		
		Usuario usuarioSalvo = this.repository.save(usuario);
		assertNotNull(usuarioSalvo);
	}
	
	@Test
	public void test4() {
		Usuario usuario = new Usuario();
		usuario.setId(1);
		usuario.setEmail("usuario@usuarios.com");
		usuario.setNome("Usuário de teste");
		usuario.setMatricula("111111111");
		usuario.setTelefone("+5585988888888");
		
		Usuario usuarioSalvo = this.repository.save(usuario);
		assertNotNull(usuarioSalvo);
	}
	
	@Test
	public void test5() {
		Usuario usuario = new Usuario();
		usuario.setId(1);
		usuario.setEmail("usuario@usuarios.com");
		usuario.setNome("Usuário de teste");
		usuario.setMatricula("111111111");
		usuario.setTelefone("+5585988888888");
		
		Usuario usuarioSalvo = this.repository.save(usuario);
		assertNotNull(usuarioSalvo);
	}

}

