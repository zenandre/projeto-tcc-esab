package br.com.zenandre.tccesab.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
public class Usuario implements Serializable {

	private static final long serialVersionUID = 8066615613873572202L;
	
	@Id
	private Integer id;
	@NotNull(message="Nome é obrigatório")
	@Column(nullable=false)
	private String nome;
	@Email(message="Campo deve possuir o formato: email@dominio.algo")
	@Column(nullable=false)
	private String email;
	private String telefone;
	private String matricula;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	

}
