package br.com.zenandre.tccesab.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.zenandre.tccesab.domain.Usuario;
import br.com.zenandre.tccesab.repository.UsuarioRepository;

@Service
public class UsuarioService {
	@Autowired
	private UsuarioRepository repo;
	
	
	public Usuario insert(Usuario usuario) {
		return this.repo.save(usuario);
	}
	
	public Usuario update(Usuario usuario) {
		if (usuario.getId() != null) {
			return this.repo.save(usuario);
		}
		return null;
	}
	
	public Usuario findById(Integer id) {
		return this.repo.findById(id).orElse(null);
	}
	
	public void delete(Usuario usuario) {
		this.repo.delete(usuario);
	}
	
	public List<Usuario> getListUsuario(){
		return this.repo.findAll();
	}

}
