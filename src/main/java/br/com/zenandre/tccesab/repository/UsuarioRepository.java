package br.com.zenandre.tccesab.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.zenandre.tccesab.domain.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

}
