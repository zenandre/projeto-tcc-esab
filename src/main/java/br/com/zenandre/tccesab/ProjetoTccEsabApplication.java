package br.com.zenandre.tccesab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoTccEsabApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoTccEsabApplication.class, args);
	}

}

